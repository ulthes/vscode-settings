﻿foreach($line in Get-Content .\extensions) {
    $command = 'code --install-extension ' + $line
    $composedMessage = [string]::Format("Executing '{0}'", $command)
    Write-Host $composedMessage -ForegroundColor Cyan
    iex $command
}